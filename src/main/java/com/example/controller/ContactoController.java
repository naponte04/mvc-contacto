package com.example.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.bean.Contacto;
import com.example.service.ContactoService;

@Controller
@RequestMapping("/contactos")
public class ContactoController {

	@Autowired
	private ContactoService contactoService;
	
	@GetMapping
	public String obtenerContactos(Model model){
		List<Contacto> listaContactos = contactoService.obtenerContactos(); 
		model.addAttribute("contactos", listaContactos);
		return "lista";
	}
	
	@GetMapping("/form")
	public String verForm(Model model, 
			@RequestParam(name="accion", required = false, defaultValue="agregar") String accion,
			@RequestParam(name="id", required = false) String id) {
		
		
		Contacto contacto = null;
		if ("agregar".equalsIgnoreCase(accion)) {
			contacto = new Contacto();
		} else if ("editar".equalsIgnoreCase(accion)) {
			if (id == null) {
				//retornar una excepcion
			}
			contacto = contactoService.obtenerContacto(Integer.parseInt(id));
		}
		model.addAttribute("contacto", contacto);
		return "agregar";
	}
	
	@PostMapping
	public String agregarContactos(@ModelAttribute("contacto") Contacto contacto){
		Contacto contactoResponse = contactoService.agregarContacto(contacto); 
		return "redirect:/contactos";
	}
	
}
